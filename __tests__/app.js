"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const helpers = require("yeoman-test");

describe("generator-json-server:app", () => {
  beforeAll(() => {
    return helpers.run(path.join(__dirname, "../generators/app"));
  });

  it("creates files", () => {
    assert.file(["mock-server/db.json", "mock-server/routes.json"]);
  });
});
