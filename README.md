> json-server project boilerplate

## Installation

```bash
yarn global add yo
yarn global add @gburnett/generator-json-server
```

Then generate your new project:

```bash
yo @gburnett/json-server
```

## License

MIT © [gburnett]()
