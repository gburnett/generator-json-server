"use strict";
const Generator = require("yeoman-generator");
const extend = require("deep-extend");

module.exports = class extends Generator {
  writing() {
    const templates = ["mock-server/routes.json", "mock-server/db.json"];
    templates.forEach(template =>
      this.fs.copy(this.templatePath(template), this.destinationPath(template))
    );

    const pkg = this.fs.readJSON(this.destinationPath("package.json"), {});
    const javascriptPkg = this.fs.readJSON(
      this.templatePath("json-server-package.json")
    );

    extend(pkg, javascriptPkg);

    this.fs.writeJSON(this.destinationPath("package.json"), pkg);
  }

  install() {
    this.yarnInstall(["json-server"], { dev: true });
  }
};
